# Role Based Access Control によるユーザの権限管理
[Role-based Access Control](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/authentication_and_authorization/using-rbac) を用いてユーザに Project や Pod の参照や作成権限を与えます。
また、グループに対するユーザの作成や削除だけでユーザへの権限管理を完結できる様にユーザではなくグループに権限を与えます。

# Table of Contents
- [1. Project と Pod の参照権限](#1-project-と-pod-の参照権限)
  - [1.1. Project の作成](#11-project-の作成)
  - [1.2. ClusterRole と RoleBinding の作成](#12-clusterrole-と-rolebinding-の作成)
  - [1.3. Project の参照](#13-project-の参照)
  - [1.4. Role と RoleBinding の作成](#14-role-と-rolebinding-の作成)
  - [1.5. Pod の参照](#15-pod-の参照)
- [2. Pod の作成権限](#2-pod-の作成権限)
  - [2.1. RoleBinding の作成](#21-rolebinding-の作成)
  - [2.2. Pod の作成](#22-pod-の作成)
- [3. グループに対する権限の設定](#3-グループに対する権限の設定)
  - [3.1. グループの作成](#31-グループの作成)
  - [3.2. ClusterroleBinding の作成](#32-clusterrolebinding-の作成)
  - [3.3. グループに設定された権限の確認](#33-グループに設定された権限の確認)

---
# 1. Project と Pod の参照権限
まず `${USER}-dev` ユーザに `${USER}-rbac` Project を参照させる権限を与え、このユーザが `${USER}-rbac` Project を参照させる権限だけを許可します。
許可するリソースやオペレーションを定義する `ClusterRole` を作成し、どのユーザに権限を与えるか定義する `RoleBinding` を作成します。

次に `${USER}-dev` ユーザに `${USER}-rbac` Project 内で Pod を参照させる権限を与え、このユーザが Pod を参照できるよう許可します。
ここでは `ClusterRole` でなく `Role` を利用し、特定の Project 内にとどまる権限を許可します。

## 1.1. Project の作成
OpenShift Web Console にログインし、規定の認証情報を入力します。
![handson-0](./images/rbac/login_userx.png)

左側メニューから **Home** → **Projects** を選択します。
![handson](./images/rbac/menu_projects.png)

画面右側の **Create Project** を選択します。
![handson](./images/rbac/create_project.png)

Name に `${USER}-rbac` を入力し、**Create** を選択します。

**`${USER}-` で始まる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
![handson](./images/rbac/create_userx-rbac_project.png)

Project が作成されることを確認します。
![handson](./images/rbac/userx-rbac_project_view.png)

次に Pod を作成します。
左側メニューから **Workloads** → **Pods** を選択します。
![handson](./images/rbac/menu_pods.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create Pod** を選択します。
![handson](./images/rbac/create_pod.png)

変更を加えずに画面下の **Create** をクリックします。
![handson](./images/rbac/create_example_pod.png)

Pod が作成されることを確認します。
![handson](./images/rbac/example_pod_view.png)

## 1.2. ClusterRole と RoleBinding の作成
許可するリソースやオペレーションを定義する `ClusterRole` とどのユーザや Namespace に権限を与えるか指定する `RoleBinding` を作成します。
まず Project の参照を許可する `ClusterRole` を作成します。

左側メニューから **User Management** → **Roles** を選択します。
![handson](./images/rbac/menu_roles.png)

画面右上の **Create Role** を選択します。
![handson](./images/rbac/create_role.png)

編集画面が表示され、サンプルの設定が表示されますが、ここではこの設定は利用せずに次の設定を利用します。
`name: ${USER}-view-project` を `user1-view-project` の様にログインするユーザ名に置き換えます。
```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: ${USER}-view-project
rules:
  - apiGroups:
      - ''
    resources:
      - namespaces
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - 'project.openshift.io'
    resources:
      - projects
    verbs:
      - get
      - watch
      - list
```

`ClusterRole` の内容は `rules` 配下の `resources` に対象とするリソースを指定、ここでは `projects` と `namespaces` リソースを指定しています。
`verbs` に `get` や `list` の様な許可する REST API のオペレーションを指定、ここでは参照権限 (`get`, `watch`, `view`) を付与します。
OpenShift では Kubernetes の Namespace を拡張し、Project として利用するため、 Project の参照のために Namespace の参照権限も付与しています。

編集後に **Create** をクリックします。
![handson](./images/rbac/userx-view-project_view.png)

次に `RoleBinding` を作成することで、先程作成した `ClusterRole` をユーザに付与します。

左側メニューから **User Management** → **RoleBindings** を選択します。
![handson](./images/rbac/menu_rolebindings.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create binding** を選択します。
![handson](./images/rbac/create_binding.png)

以下の内容を入力し **Create** を選択します。

**`${USER}-` ではじまる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
* Binding type: Namespace role binding (RoleBinding) を選択
* RoleBinding Name: view-project-rolebinding
* Role name: ${USER}-view-project
* Subject: 
  - Subject name: ${USER}-dev
![handson](./images/rbac/create_rolebinding_view-project1.png)
![handson](./images/rbac/create_rolebinding_view-project2.png)

`RoleBinding` が作成されることを確認します。
![handson](./images/rbac/view-project-rolebinding_view.png)

## 1.3. Project の参照
与えられた権限を検証するため、`${USER}` から `${USER}-dev` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx.png)

`${USER}-dev` ユーザでログインします。
![handson](./images/rbac/login_userx-dev.png)

**Topology** を選択すると `${USER}-rbac` Project が表示されます。
`${USER}-dev` ユーザに `${USER}-rbac` Project の参照権限を与えたため、このユーザはこの Project を参照することができます。
![handson](./images/rbac/topology_view.png)

次に `${USER}-rbac` Project を選択し, **Workloads** -> **Pods** を選択すると以下のような画面が表示されます。
![handson](./images/rbac/topology_view_project.png)

この Project に Pod を作成したので、本来は Pod が表示されるはずですが、"pods is forbidden..." と表示されます。
これは Project の参照権限だけを与えましたが Pod を参照させる権限までは与えていないためです。

次にこの Pod を参照できるよう `${USER}-dev` に必要な権限を追加します。

## 1.4. Role と RoleBinding の作成
次に `${USER}-dev` ユーザに `${USER}-rbac` Project 内で Pod を参照させる権限を与え、このユーザが Pod を参照できるよう許可します。
ここでは `ClusterRole` でなく `Role` を利用し、特定の Project 内にとどまる権限を許可します。

まず `${USER}-dev` から `${USER}` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx-dev.png)

`${USER}` ユーザでログインします。
![handson](./images/rbac/login_userx.png)

左側メニューから **User Management** → **Roles** を選択します。
![handson](./images/rbac/menu_roles.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create Role** を選択します。
![handson](./images/rbac/create_role.png)

このサンプルは Pod の参照権限を付与するため、`name` を `name : view-pods-role` に書き換え **Create** を選択します。
![handson](./images/rbac/create_view-pods-role.png)

`Role` が作成されます。
![handson](./images/rbac/view-pods-role_view.png)

次に `RoleBinding` を作成し `${USER}-dev` に権限を付与します。
左側メニューから **User Management** → **RoleBindings** を選択します。
![handson](./images/rbac/menu_rolebindings.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create binding** を選択します。
![handson](./images/rbac/create_binding.png)

以下の内容を入力して、**Create** を選択します。

**`${USER}-` ではじまる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
* Binding type : Namespace role binding (RoleBinding)
* RoleBinding
  * Name: view-pods-rolebinding
  * Namespace: ${USER}-rbac
* Role
  * Role name: view-pods-role
* Subject: User
  * Subject name: ${USER}-dev
![handson](./images/rbac/create_rolebinding_view-pods1.png)
![handson](./images/rbac/create_rolebinding_view-pods2.png)

画面で `RoleBinding` が作成されることを確認します。
![handson](./images/rbac/rolebinding_view-pods_view.png)

## 1.5. Pod の参照
与えられた権限を検証するため、`${USER}` から `${USER}-dev` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx.png)

`${USER}-dev` ユーザでログインします。
![handson](./images/rbac/login_userx-dev.png)

**Topology** から `${USER}-rbac` Project を選択すると、先程は "No resource found" と表示されましたが、今度は Pod が表示されます。
![handson](./images/rbac/topology_view2.png)


これで `${USER}-dev` ユーザは `${USER}-rbac` Project を参照して、この Project にデプロイされた Pod を参照できるようになりました。
Project の参照には `ClusterRole` と `Rolebinding` を設定し、Project の参照権限を与え、Pod の参照は `Role` と `RoleBinding` を設定しました。
ただし、このように不足する権限をつど見つけて設定することは手間がかかるため、OpenShift ではデフォルトで様々な用途に合わせて `ClusterRole` がいくつも用意されています。
次はこの `ClusterRole` を用います。

---
# 2. Pod の作成権限
デフォルトで提供される `ClusterRole` を利用し `${USER}-dev` に Pod の作成権限を与えます。

デフォルトで提供される `ClusterRole` は多数ありますが、よく利用される一例を紹介します。
今回は Pod の作成権限を付与するため `edit` を利用します。
* cluster-admin : 全ての Project を対象に全てのオペレーションを実行できる
* admin: Project 内の `Quota` 以外のリソースを参照及び変更できる
* edit: Project 内の `Quota`, `Role`, `Rolebinding` 以外のリソースを参照及び変更できる
* view: Project 内のほぼ全てのリソースを参照できる
* basic-user: Project や User などの基本的な情報を参照できる

`cluster-admin` は全ての権限が許可され最も強い権限を持ち、一方で `basic-user` は限定的な権限だけ許可されます。
そのため通常は `admin`, `edit`, `view` を選択するケースが多く、より詳細な権限が必要な場合はその他の `ClusterRole` を利用する、またはカスタマイズして利用します。
詳細は [デフォルトのクラスターロール](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/authentication_and_authorization/using-rbac#default-roles_using-rbac) を参照下さい。

## 2.1. RoleBinding の作成
まず `${USER}-dev` から `${USER}` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx-dev.png)

`${USER}` ユーザでログインします。
![handson](./images/rbac/login_userx.png)

`RoleBinding` を作成し `${USER}-dev` に `edit` 権限を与えます。

左側メニューから **User Management** → **RoleBindings** を選択します。
![handson](./images/rbac/menu_rolebindings.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create binding** を選択します。
![handson](./images/rbac/create_binding.png)

以下の内容を入力し **Create** を選択します。

**`${USER}-` ではじまる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
* Binding type : Namespace role binding (RoleBinding)
*  RoleBinding
  * Name: edit-rolebinding
  * Namespace: ${USER}-rbac
* Role
  * Role name: edit
* Subject: User
  * Subject name: ${USER}-dev
![handson](./images/rbac/create_edit-rolebinding1.png)
![handson](./images/rbac/create_edit-rolebinding1.png)

画面で `RoleBinding` が作成されることを確認します。
![handson](./images/rbac/edit-rolebinding_view.png)

## 2.2. Pod の作成
与えられた権限を検証するため、`${USER}` から `${USER}-dev` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx.png)

`${USER}-dev` ユーザでログインします。
![handson](./images/rbac/login_userx-dev.png)

メニュー左上の **Developer** プルダウンから **Administorator** を選択、管理者画面に切り替えます。
![handson](./images/rbac/console_administrator.png)

左側メニューから **Projects** "を選択します。
![handson](./images/rbac/menu_projects.png)

左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create Pod** を選択します。
![handson](./images/rbac/create_pod.png)

最初に作成した `example` Pod と名前が重複すると作成できないため、`name` を `example2` に修正し、**Create** を選択します。
![handson](./images/rbac/create_example2_pod.png)

Pod が作成されます。
![handson](./images/rbac/create_example2_pod_view.png)

このようにデフォルトで提供される `ClusterRole` のうち `edit` を付与することで Pod を作成する権限が与えられ、Pod を作成することができます。

---
# 3. グループに対する権限の設定
ここまでは `${USER}-dev` ユーザの様に特定のユーザに権限を与えましたが、実際の運用では特定のユーザでなくグループに設定する方が効率的です。
ユーザでなくグループに権限を与えることで、ユーザの作成や削除においてそのつどユーザに権限を与える必要はなく、グループにユーザを追加や削除するだけで運用することができます。

このセクションではグループを作成し、そのグループにクラスタ全体の参照を許可する `cluster-reader` 権限を与えます。
その後、グループにユーザを登録して登録したユーザに `cluster-reader` 権限が付与されることを確認します。

## 3.1. グループの作成
まず `${USER}-dev` から `${USER}` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx-dev.png)

`${USER}` ユーザでログインします。
![handson](./images/rbac/login_userx.png)

左側メニューから **User Management** → **Groups** を選択、右側の **Create Group** を選択します。
![handson](./images/rbac/create_group.png)

以下の内容を入力し **Create** を選択します。
**`${USER}-` ではじまる文字列は `user1-` の様にログインするユーザ名に置き換えます。**
```
apiVersion: user.openshift.io/v1
kind: Group
metadata:
  name: ${USER}-group
users:
 - ${USER}
 - ${USER}-dev
```

グループが作成されることを確認します。
![handson](./images/rbac/userx-group_view.png)

## 3.2. ClusterroleBinding の作成
`ClusterRoleBinding` を作成し、`cluster-reader` を `${USER}-group` に付与します。

左側メニューから **User Management** → **RoleBindings** を選択します。
左上の Project が `${USER}-rbac` であることを確認し、画面右上の **Create binding** を選択します。
![handson](./images/rbac/create_binding.png)

以下の内容を入力し **Create** を選択します。
* Binding type : Cluster-wide role binding (ClusterRoleBinding)
* RoleBinding Name: ${USER}-cluster-reader
* Role name: cluster-reader
* Subject: Group
* Subject name: ${USER}-group

![handson](./images/rbac/create_rolebinding.png)

`RoleBinding` が作成されることを確認します。
![handson](./images/rbac/create_rolebinding_view.png)

## 3.3. グループに設定された権限の確認
与えられた権限を検証するため、`${USER}` から `${USER}-dev` ユーザに切り替えます。

画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/rbac/logout_userx.png)

`${USER}-dev` ユーザでログインします。
![handson](./images/rbac/login_userx-dev.png)

メニュー左上の **Developer** プルダウンから **Administorator** を選択、管理者画面に切り替えます。
![handson](./images/rbac/console_administrator.png)

`cluster-reader` を付与することで権限を付与する前には表示されない **Overview** や他のユーザが作成した Project を参照できるようになります。
![handson](./images/rbac/overview.png)

この様にグループに権限を与えることで直接ユーザに権限を与える必要はなくなり、グループに対するユーザの作成や削除だけでユーザへの権限管理を完結させることができます。
今回は `${USER}-group` といった仮の名称でグループを作成しましたが、例えば実際の運用では次のように割り当てる環境やグループを表現するマトリクスを作成し、各グループに権限を与える方針が一般的です。
権限の割り当てはあくまで一例となり、グループの名称や割り当てる権限は要件に応じて検討する必要があります。

アプリケーション開発グループ (`developers`) に割り当てる環境毎の権限
| Namespace/Group | admin | edit | view |
| --------------- | ----- | ---- | ---- |
| development | o | - | - |
| staging | - | o | - |
| production | - | - | o |

アプリケーション運用グループ (`operations`) に割り当てる環境毎の権限
| Namespace/Group | admin | edit | view |
| --------------- | ----- | ---- | ---- |
| development | - | - | o |
| staging | - | - | o |
| production | o | - | - |

以上でこのセクションは終了です。

