# OpenShift Monitoring によるクラスタとアプリケーションの監視
クラスタ管理者とアプリケーション運用者の観点から OpenShift クラスタやクラスタにデプロイされたアプリケーションを監視する方法を確認します。

OpenShift は監視を提供するコンポーネントに [OpenShift Monitoring](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/monitoring/index#monitoring-overview) を内蔵し、メトリクスを参照するためのダッシュボードに [Monitoring Dashboard](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/monitoring/index#reviewing-monitoring-dashboards) を提供します。
この Monitoring Dashboard を利用し、クラスタ管理者の観点では OpenShift クラスタのステータスやシステムリソースなどの確認方法を、アプリケーション運用者の観点ではアプリケーションのステータスやシステムリソースの確認方法を、またサンプル用アプリケーションをデプロイし、実際にアラートを設定しアラートの通知を確認します。

最後に OpenShift クラスタにデプロイされたアプリケーションに障害が起きた際のトラブルシューティングを想定し、メモリの高負荷による擬似的な障害を起こし、システムリソースの使用量やアプリケーションのログを調査します。
OpenShift は OpenShift Monitoring と同様にログ管理を提供するコンポーネントに EFK Stack をベースとした [OpenShift Logging](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/logging/cluster-logging) を内蔵し、システムコンポーネントやアプリケーションのログを参照するためのダッシュボードに [Kibana](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/logging/cluster-logging-visualizer-using) を提供します。
ユーザがデプロイする Pod のログは Fluentd により収集され、Elasticsearch に格納、Kibana から参照することができます。
この Logging Dashboard から Pod のログを参照し、アプリケーションの状態を確認します。

# Table of Contents
- [1. Monitoring Dashboard の利用](#1-monitoring-dashboard-の利用)
  - [1.1. Monitoring Dashboard へのアクセス](#11-monitoring-dashboard-へのアクセス)
  - [1.2. クラスタ全体のサマリ](#12-クラスタ全体のサマリ)
  - [1.3. Project (Namespace) のサマリ](#13-project-namespace-のサマリ)
  - [1.4. アラートの通知](#14-アラートの通知)
  - [1.5. ダッシュボードの利用](#15-ダッシュボードの利用)
  - [1.6. PromQL を用いたメトリクスの取得](#16-promql-を用いたメトリクスの取得)
- [2. アプリケーションの監視](#2-アプリケーションの監視)
  - [2.1. テスト用アプリケーションのデプロイ](#21-テスト用アプリケーションのデプロイ)
  - [2.2. メトリクスの収集と確認](#23-メトリクスの収集と確認)
  - [2.3. アラートルールの設定と通知の確認](#24-アラートルールの設定と通知の確認)
- [3. アプリケーション障害時の解析](#3-アプリケーション障害時の解析)
  - [3.1. アプリケーションのデプロイ](#31-アプリケーションのデプロイ)
  - [3.2. アプリケーションのトラブルシューティング](#32-アプリケーションのトラブルシューティング)

---
# 1. Monitoring Dashboard の利用
OpenShift にデフォルトで組み込まれる Monitoring Dashboard にアクセスし、OpenShift クラスタのステータスやシステムリソースに関するメトリクスを参照し、俯瞰的にクラスタ全体を監視するためにどの様な統計情報が表示されるか、また個々のメトリクスを詳細に参照する手順を確認します。

## 1.1. Monitoring Dashboard へのアクセス
OpenShift は管理者と開発者向けに Web Console を提供しますが、OpenShift クラスタのステータスや関連するメトリクスを参照するには管理者用コンソール (Administrator Perspective) にアクセスする必要があります。
アクセスに [`cluster-admin`](https://docs.openshift.com/container-platform/4.10/monitoring/reviewing-monitoring-dashboards.html) と呼ばれるクラスタ管理権限が必要です。
ここでは `cluster-admin` 権限を付与されたユーザでアクセスします。

WEB ブラウザから OpenShift Web Console にログインします。
![](./images/monitoring/login.png)

 ログイン後に画面左上のパースペクティブ変更ドロワーメニューから **Administrator (管理者)** を選択、管理者向けコンソールを表示します。
![](./images/monitoring/console_admin.png)

## 1.2. クラスタ全体のサマリ
**Home** -> **Overview** を選択し、クラスタの現在の状態のサマリー画面を表示し、アラートおよびリソースの使用状況を確認します。
![](./images/monitoring/console_overview.png) 

例えば Status 欄に次のような項目が含まれ、異常がある場合は "Warning" や "Critical" アイコンが表示されます。
- Cluster: クラスタの正常性
- Controle Plane: API Server など Controle Plane コンポーネントの正常性やレスポンスレート
- Operators: インストールされた各 Operator の正常性やステータス異常の有無
- Insights: クラスタに潜在的な問題があるかどうか

## 1.3. Project (Namespace) のサマリ
**Home** -> **Projects** -> **<任意の Project 名>** を選択し、 Project ごとのステータスやリソースの使用状況を表示します。
例えば検索バーに "openshift-monitoring" を入力し openshift-monitoring Project を選択することで、 Monitoring を構成する Pod のリソースが表示されます。
![](./images/monitoring/console_overview_project.png) 

図の例の Overview タブでは、"openshift-monitoring" Project に含まれる `Pod` や `Deployment` などの各種リソースの情報や、1時間単位、6時間単位、24時間単位、で Project単位での CPUやメモリ利用率などの概況を確認できます。
![](./images/monitoring/console_overview_project_detail.png)

Overview、Details、YAML、Workloads、RoleBindings、などの各タブを選択して確認します。

## 1.4. アラートの通知
**Observe** -> **Alerts** を選択し、検出されたアラートの一覧を表示します。
下図の例では、クラスタアップデートが利用可能であることを示す `UpdateAvailable`、AlertManager が機能していることの確認のための `Watchdog`、外部システムへの通知が設定されていないことを示す `AlertmanagerReceiverNotConfigured` が検出されています。
![](./images/monitoring/console_overview_observe_alert.png)

また、 "Warning" や "Critical" といった重大度のアラートは Web コンソールの右上にあるベルのアイコンを選択して通知欄で確認することもできます。
![](./images/monitoring/console_overview_alert_notification.png)

<!--
> --- 以下は講師が実施します ---
>
>通知を受け取る外部システムの設定が行われていない場合、 通知欄もしくは **Home** -> **Overview** を選択し、  AlertmanagerReceiverNotConfigured アラートについて Configure を選択すると外部システムへの通知設定が行なえます。Administration -> Cluster Settings -> Global Configuration -> Alertmanager に移動することでも設定可能です。
>
>外部システムへの通知設定はアラートレシーバを作成することで設定します。
>アラートレシーバには通知を行うアラートのラベルによるフィルタリング設定、レシーバタイプとして通知する外部システムを設定します。
>
>デフォルトで定義されているレシーバは以下の 3 種類です。ただし、これらのレシーバには通知先システムを指定するレシーバタイプが設定されていないため、設定を行っていきます。 **(講師が実施します)**
>
>-  Critical : 重大度が Critical のアラートを通知
>-  Default : 他のレシーバーがキャッチしていないすべてのアラートを通知
>-  Watchdog : 常にアラート通知される AlertManager が機能していることの確認通知
>
>今回はデフォルトで定義されているレシーバに Slack Webhook を設定していきます。
>
>アラートの通知設定を行う場合の考え方として、早急に対処が必要な重大度の高いアラートについては PagerDuty 等により電話、SMS などによる通知を行い、それ以外の重要度の高くないアラートについては Slack に通知しておき、 GitLab Issue に登録して後日対応するなど、極力不要なオペレーションを発生させない工夫をすることが推奨されます。
-->

## 1.5. ダッシュボードの利用
**Observe** -> **Dashboards** を選択し、クラスタの主要なメトリクスが化されたダッシュボードを確認します。
このダッシュボードでは個々のメトリクスではなく、API Server や Etcd といったコンポーネント毎に集約されたメトリクスのグラフが表示されます。
![](./images/monitoring/console_overview_observe_dashboards.png)

下図のように Dashboard セクションから任意のフィルターオプションを選択し、対象を絞ります。
![](./images/monitoring/console_overview_observe_dashboards_filter.png)

**Kubernetes / Networking / Namespaces (Pods)** を選択、次に任意の Project を選択することで、Project に属する Pod のネットワーク帯域情報が表示されます。
![](./images/monitoring/console_overview_observe_dashboards_filter_detail.png)

この様にダッシュボードは Etcd やクラスタといったコンポーネント毎や Pod 毎などカテゴリ毎に俯瞰して、システムリソースや性能指標などを参照する場合に利用します。

## 1.6. PromQL を用いたメトリクスの取得
まず、サンプルの PromQL クエリを実行して、メトリクスを取得、PromQL のイメージをつかみます。

**Observe** -> **Metrics** を選択し、**Insert example query** をクリックし、**Run queries** をクリックし、グラフが表示されることを確認します。
`Expression` と表示されたサーチバーに以下のクエリが挿入され、取得したメトリクス値と時系列のグラフが表示されます。
![](./images/monitoring/console_overview_observe_metrics.png)

![](./images/monitoring/console_overview_observe_metrics_sample.png)

このクエリは、24 時間以内に発火(firing)されたアラートの回数をアラートごとに集計し、降順でソートして表示します。
```
sort_desc(sum(sum_over_time(ALERTS{alertstate="firing"}[24h])) by (alertname))
```

クエリの表現には [Prometheus Query Language](https://prometheus.io/docs/prometheus/latest/querying/basics/) (PromQL) が用いられ、RDBMS などの SQL よりも簡素な表現でソートや合計 (Sum) を定義し、メトリクスを整形することができますが、Prometheus は時系列データモデルを用いてデータを保存するため、PromQL クエリは従来の SQL とは根本的に異なります。

PromQL は Key/Value 形式で取得する対象をフィルタリングしたり、取得する時間の範囲を指定したりすることができるため、順を追って取得するメトリクスをより詳細にフィルタリングします。
PromQL の表記は `{key=value}[Time Duration]` となり、最初の `key=value` でメトリクスをフィルタリングし、次の `[Time Duration]` で時間の範囲を指定します。

例えば `http_requests_total{job="prometheus"}[5m]` は `http_requests_total` メトリクスを対象に `job` ラベルに `prometheus` がセットされた値をフィルタリングし、最後の 5 分間を範囲とします。
取得できるデータの種類は次のとおりになります。
- Instant vector：時間範囲を指定しない場合に取得できる、単一タイムスタンプでの値
- Range vector：時間範囲を指定した場合に取得できる、複数のタイムスタンプでの値のセット
- Scalar: 浮動小数点数で表現される数値。後述する演算子で Instant vector の値と Scalar 値を演算する事が可能

PromQL の詳細は [QUERYING PROMETHEUS](https://prometheus.io/docs/prometheus/latest/querying/basics/#time-series-selectors) に、設定例は [QUERY EXAMPLES](https://prometheus.io/docs/prometheus/latest/querying/examples/) に公開されています。

では実際に PromQL の詳細な使い方について、ラベルの指定方法、時間範囲の指定方法、演算子、関数の利用方法を確認します。
まず API Server を呼び出すサンプル用アプリケーションをデプロイし、API Server 性能指標として GET リクエストの合計数やコンテナのメモリ使用量を PromQL クエリを実行して取得します。

次に PromQL クエリを実行して API Server への HTTP リクエストの合計数 (`apiserver_request_total`) を取得します。

まず **Observe** -> **Metrics** を選択し、画面下部の `Expression` と表示されたサーチバーに `apiserver_request_total` を入力、**Run queries** をクリックし、グラフが表示されることを確認します。
![](./images/monitoring/console_overview_observe_metrics.png)

次にラベルを指定します、同じクエリに条件を追加し、`apiserver_request_total{}` 内にラベルを指定して API Server に対する GET リクエストの合計数から  `Deployment` リソースだけをフィルタリングします。
サーチバーに `apiserver_request_total{code="200",endpoint="https",job="apiserver",namespace="default",resource="deployments",service="kubernetes",verb="GET"}` を入力し、**Execute** を選択しクエリを実行、**Table** タブから指定した条件に合致するメトリクスだけが表示されることを確認します。
![](./images/monitoring/prometheus_ui_promql_label.png)

さらに同じクエリに条件を追加し、`[]` 内に時間の範囲を指定して、過去 5 分間に採取した値のセット (Range vector) を対象とします。
サーチバーに `apiserver_request_total{code="200",endpoint="https",job="apiserver",namespace="default",resource="deployments",service="kubernetes",verb="GET"} [5m]` を入力し、 **Execute** を選択しクエリを実行、
**Table** タブにから直近 5 分間の結果だけが表示されることを確認します。
![](./images/monitoring/prometheus_ui_promql_label_5m.png)

<!-- Developer Perspective > Observe > Metrics から Memory Usage を選択すればクエリを発行するので、覚える必要がない
最後にコンテナのメモリ使用量 (`container_memory_working_set_bytes`) を取得します。
メモリ使用量を取得するにあたり、演算子と関数を利用、PromQL で利用できる演算子と関数は次のとおりになります。
- “+” などの算術演算子
- “>=” などの比較演算子
- “sum” などの集計演算子 & “by” 句、”without” 句による集計単位の指定
- “floor” などの関数を利用可能

集計演算子 (`sum( ~ ) by (pod_name)`) でコンテナのメモリ使用量を Byte 単位で Pod ごとに集計、 Scalar 値による演算 (`/ 1024 / 1024`) を指定して、MiB 単位に換算します。

サーチバーに `sum(container_memory_working_set_bytes{namespace="monitoring-example-${USER}",container!="",image!=""} / 1024 / 1024) by (pod)` を入力し、**Execute** を選択しクエリを実行、**Table** タブにコンテナのメモリ使用量の合計値が出力されることを確認します。
![](./images/monitoring/expression_prometheus_promql_sum.png)
**Graph** タブに切り替え、グラフが表示されることを確認します。
 ![](./images/monitoring/expression_prometheus_promql_sum_graph.png)

次に同じクエリに条件を追加し、関数 `floor` を利用することで小数値を切り捨てた MiB 単位で表示します。

サーチバーに `floor(sum(container_memory_working_set_bytes{namespace="monitoring-example-<開発ユーザ ID>",container!="",image!=""} / 1024 / 1024) by (pod))` を入力し、**Execute** を選択しクエリを実行、**Table** タブに小数点以下が切り捨てられたメモリ使用量が出力されることを確認します。
![](./images/monitoring/expression_prometheus_promql_sum.png)
-->

---
# 2. アプリケーションの監視
サンプル用アプリケーションをデプロイして HTTP リクエスト数のメトリクスを取得、このメトリクスをもとにアラートを設定、実際にアラートが通知されることを確認します。

## 2.1. テスト用アプリケーションのデプロイ
テスト用アプリケーションを Web Terminal からデプロイします。

ここでは `${USER}` ユーザでなく、`${USER}-ops` ユーザを利用するため、まず画面右上から **${USER}** → **Log out** を選択し、ログアウトします。
![handson](./images/monitoring/logout.png)

ログインプロンプトが表示された後 `${USER}-ops` ユーザでログインします。
![handson](./images/monitoring/login_userops.png)

Web Terminal を起動していない場合、コンソール右上から Web Terminal を起動します。
![](./images/monitoring/expression_terminal_run.png)

環境変数を設定します。
**Web Terminal のセッションは 15 分で切れるため、セッションが切れても環境変数が設定されるよう `~/.bashrc` に書き込みます。**
```
cat <<EOF>> ~/.bashrc
export USER=\$(oc whoami)
EOF
. ~/.bashrc
```

`${USER}` Project に `Deployment` を作成し、`Deployment` にリソースをセットします。
Project 自体は既に作成しているため、作成する必要はありません。
```
oc -n ${USER} create deployment --image=quay.io/brancz/prometheus-example-app:v0.2.0 prometheus-example-app
oc -n ${USER} set resources deployment prometheus-example-app --requests=cpu=100m,memory=256Mi --limits=cpu=200m,memory=512Mi
```

Prometheus にメトリクスを収集させるため `Service` を作成します。理由は後述します。
```
cat << EOF | oc apply -f -
apiVersion: v1
kind: Service
metadata:
  labels:
    app: prometheus-example-app
  name: prometheus-example-app
  namespace: ${USER}
spec:
  ports:
  - name: web
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: prometheus-example-app
EOF
```

`Deployment` から `Pod` が生成されることを確認します。
 `-l` フラグでラベルを指定することでリストする対象をフィルタリングします。
```
oc -n ${USER} get pod -l app=prometheus-example-app

# 出力例
NAME                                      READY   STATUS    RESTARTS   AGE
prometheus-example-app-7b6b949d79-4qrkp   1/1     Running   0          4m46s
```

サンプル用アプリケーションは Prometheus 形式のメトリクスを `http://<URL>/metrics` で公開する様に設定しており、Prometheus はメトリクスをこのエンドポイントから収集するため、メトリクスが公開されているか確認します。
`oc rsh` コマンドで Pod にログインし、`wget` でローカルからアプリケーションの正常性とメトリクスが公開されていることを確認します。
```
oc -n ${USER} rsh deploy/prometheus-example-app
```

アプリケーションが正常な場合、次のメッセージが出力されます。
```
wget -q -O - localhost:8080/

出力例: 
Hello from example application.
```

存在しないパス (`/err`) にアクセスすると HTTP ステータスコード 404 を返します。
```
wget -q -O - localhost:8080/err

出力例: 
wget: server returned error: HTTP/1.1 404 Not Found
```

`/metrcs` にアクセスするとメトリクスが出力されます。
ローカルホストと Service 経由のパターンを確認します。
出力例はいずれも同じです。
```
wget -q -O - localhost:8080/metrics
wget -q -O - prometheus-example-app:8080/metrics

出力例:
# HELP http_requests_total Count of all HTTP requests
# TYPE http_requests_total counter
http_requests_total{code="200",method="get"} 1
http_requests_total{code="404",method="get"} 1
# HELP version Version information about this binary
# TYPE version gauge
version{version="v0.1.0"} 1
```

Pod からログアウトします。
```
exit
```

## 2.2. メトリクスの収集と確認
Prometheus にアプリケーションが公開するメトリクスを収集させるため、`ServiceMonitor` リソースを作成し、サービスディスカバリを有効にします。
```
cat << EOF | oc apply -f -
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
    k8s-app: prometheus-example-monitor
  name: prometheus-example-monitor
  namespace: ${USER}
spec:
  endpoints:
  - interval: 30s
    port: web
    scheme: http
  selector:
    matchLabels:
      app: prometheus-example-app
EOF
```
Prometheus がメトリクスを収集する対象を検出するための サービスディスカバリは `ServiceMonitor` の `spec` フィールド配下の値が該当します。
```
spec:
  endpoints:
  - interval: 30s
    port: web # Service の `spec.ports[].name` フィールドの値を指定し、収集する際のポート番号を判定する
    scheme: http
  selector:
    matchLabels: # Service の `spec.selector` フィールドの `key: value` の値を指定し、対象とする Service 自体を判定する
      app: prometheus-example-app
```
2.1 で作成した Service
```
kind: Service
metadata:
  labels:
    app: prometheus-example-app
  name: prometheus-example-app
spec:
  ports:
  - name: web
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: prometheus-example-app
```

Developer パースペクティブに切り替え、**Observe** -> **Metrics** タブを選択し、クエリの種類に **Custom query** を選択します。
![](./images/monitoring/observe_metrics.png)

サーチバーに `http_requests_total` を入力し、メトリクスが表示されることを確認します。
![](./images/monitoring/observe_metrics_query.png)

なお CPU やメモリ使用量など主要なシステムリソースに関するクエリはデフォルトでいくつか用意されており、CPU usage や Memory usage を選択することで自動でクエリが発行されます。
そのため、これらのシステムリソースを参照する場合、上記のように Custom Query を実行する必要はありません。
![](./images/monitoring/observe_cpu_usage.png)
![](./images/monitoring/observe_memory_usage.png)

## 2.3. アラートルールの設定と通知の確認
アラートルールを設定するため、`PrometheusRule` リソースを作成し、アラートルールの詳細は `spec.groups` フィールドに定義します。
次のルールは `HttpRequestErrorRateIncrease` の名称で作成され、404 を返す HTTP リクエストのエラーレートが 5 分間で 0.3 を超えた場合にアラートを通知します。
```
cat << EOF | oc apply -f -
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: example-alert
  namespace: ${USER}
spec:
  groups:
  - name: example
    rules:
    - alert: HttpRequestErrorRateIncrease
      expr: rate(http_requests_total{code="404",job="prometheus-example-app"}[5m]) > 0.3
EOF
```

**Observe** -> **Alerts** タブを選択し、`PrometheusRule` リソースに定義したアラートを確認します。
![](./images/monitoring/prometheusrule.png)

次に実際にアラートが通知されることを確認します。Pod にログインし、`for` コマンドで 100 回ほどループさせ、HTTP リクエストを急増させます。
この際、実際には存在しないパス (`/err`) にアクセスすることでエラーレートを急増させます。
```
oc -n ${USER} rsh deploy/prometheus-example-app
for i in `seq 1 400` ; do wget -q -O - localhost:8080/err ; done ; echo 'Completed'

出力例:

wget: server returned error: HTTP/1.1 404 Not Found
...
wget: server returned error: HTTP/1.1 404 Not Found
Completed
```

**Observe** -> **Alerting** タブに戻り、`HttpRequestErrorRateIncrease` アラートのステータスが **Firing** に推移することを確認します。
![](./images/monitoring/prometheusrule_alerting.png)

また **Observe** ->**Dashboards** タブを選択すると、CPU や Memory などの使用量を確認することができ、先程の HTTP リクエストを急増させたことで、CPU 使用率が増加していることが分かります。
![](./images/monitoring/devp_dashboard.png)

実際の運用では Email や Slack など外部システムにアラートを通知する方針が一般的ですが、この例では OpenShift Web Console 上でアラートの通知を確認するまでに留めています。
OpenShift Monitoring は PagerDuty や Slack などをサポートするため、設定例は次のリンク先を参照下さい。
* [製品ガイド - 9.6 外部システムへの通知の送信](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/monitoring/index#sending-notifications-to-external-systems_managing-alerts)
* [OpenShift Blog - OpenShift 4.3: Alertmanager Configuration](https://cloud.redhat.com/blog/openshift-4-3-alertmanager-configuration)

---
# 3. アプリケーション障害時の解析
最後に OpenShift クラスタにデプロイされたアプリケーションに障害が起きた場合のトラブルシューティングを想定し、システムリソースの使用量やアプリケーションのログを調査します。
テスト用にアプリケーションをデプロイした後、メモリに負荷をかけることで Pod を再起動させ、サービスが継続できない状態を継続、アラートを検出させます。
[Monitoring Dashboard](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/monitoring/index#reviewing-monitoring-dashboards) からメモリ使用量の上昇を確認します。
次に [Kibana](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html/logging/cluster-logging-visualizer-using) からアプリケーションが正常に起動することをログから確認します。

## 3.1. アプリケーションのデプロイ
テスト用のアプリケーションをデプロイし、意図的にメモリが高負荷な状態を発生させ、コンテナを再起動させます。

Web Terminal から次のコマンドを実行し、テスト用のアプリケーションをデプロイします。
```
oc -n ${USER} new-app registry.redhat.io/jboss-webserver-5/webserver53-openjdk8-tomcat9-openshift-rhel7~https://github.com/openshiftdemos/os-sample-java-web.git
```

メモリ使用量を超えた際にコンテナを再起動させるため、リソースの要求 (`Request`) と制限 (`Limit`) を設定します。
これにより、アクセス過多などによりメモリ使用量が `Limit` の値である 512 MiB を超える場合にコンテナを再起動させます。
```
oc -n ${USER} set resources deployment os-sample-java-web --requests=cpu=100m,memory=256Mi --limits=cpu=200m,memory=512Mi
```

リソースの要求 (`Request`) と制限 (`Limit`) に関する概要は次のリンク先を参照下さい。
* [コンテナーメモリーとリスク要件を満たすためのクラスターメモリーの設定](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/nodes/index#nodes-cluster-resource-configure)
* [リソース要求とオーバーコミット](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/nodes/index#nodes-cluster-overcommit-resource-requests_nodes-cluster-overcommit)

`/dev/null` に `yes` コマンドを入力することでメモリ使用量の負荷を上げ、意図的にメモリが高負荷な状態を発生させます。
`oc rsh` による リモートシェルセッションが次の様に切れると、この時点でコンテナがメモリ高負荷により再起動しています。
コマンドの実行から終了までに 1 分程度かかる場合があります。
```
oc -n ${USER} rsh deployment/os-sample-java-web
for num in {1..40}; do /dev/null < $(yes) & done

出力例:

[1] 335
...
[40] 411

sh: xrealloc: cannot allocate 18446744071562067968 bytes (323584 bytes allocated)
```

## 3.2. アプリケーションのトラブルシューティング
Monitoring Dashboard からメモリ使用量と Logging Dashboard からアプリケーションが正常に起動していることをログから確認します。

まず Monitoring Dashboard からリソース使用量を確認します。
**Workloads** -> **Pods** を選択し、Pod の一覧が表示され、**os-sample-java-web** で始まる Pod を選択肢ます。
次に **Metrics** を選択すると、 メモリに負荷をかけた時刻を参照するとメモリ使用量が上昇した後、急激に減少していることが分かります、これはコンテナが再起動されたためです。
![](./images/monitoring/troubleshooting_memory_usage.png)

次にアプリケーションのログを確認します。
OpenShift Web コンソールにログインすると、画面上部に小さな四角が3x3で並んだアイコンが見つかります。
ここをクリックし **Logging** ボタンをクリックします。
![](./images/monitoring/troubleshooting_dashboard_login.png)

ログインには認証情報が求められるため、OpenShift Web Console にログインする際の認証情報を入力してログインします。

次のようなアクセス認証の画面が表示される場合、全ての項目ににチェックを入れたまま "Allow selected permissions" ボタンをクリックします。
![](./images/monitoring/troubleshooting_dashboard_auth.png)

Kibana Console にログインした後、Index Pattern を作成します。
Index Pattern を作成することでログを検索することができます。
Index Pattern に 下に表示された `app-<NUMBER>` を入力、**Next step** をクリックします。
![](./images/monitoring/troubleshooting_kibana_index01.png)

Time Filter field name に **@timestamp** を選択し、**Create index pattern** をクリックすると Index Pattern が作成されます。
![](./images/monitoring/troubleshooting_kibana_index02.png)

左側のメニューから **Discover** を選択し、サーチバーに次のクエリを入力し、該当する Pod からログのメッセージを検索します。
`${USER}` は `user1-ops` の様に各自のユーザ ID に置き換えます。
`kubernetes.namespace_name:${USER} AND kubernetes.flat_labels:deployment=os-sample-java-web AND message:"Running"`
ログメッセージの検索に成功し、次のように出力されることを確認します。
![](./images/monitoring/troubleshooting_kibana_output.png)

クエリの意味は次のとおりになります。
フィールドに指定できる値の一例は [](https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/logging/index#cluster-logging-exported-fields-kubernetes_cluster-logging-exported-fields) を参照下さい。
| Field       | Description |
| ----------- | ----------- |
| kubernetes.namespace_name | Project (Namespace) を指定する |
| kubernetes.flat_labels | key=value 形式のラベルを指定し、ラベルに合致する Pod を検索する |
| message:"STRING" | ログ (コンテナの STDOUT/STDERR) から検索させる文字列を指定する |

この様に Monitoring Dashboard や Logging Dashboard を用いることで、OpenShift クラスタを構成するコンポーネントのに加え、ユーザがデプロイする Pod のシステムリソースに関するメトリクスを参照したり、アプリケーションが出力するログのメッセージを検索することができ、アプリケーションに障害が起きた際にトラブルシューティングすることができます。

以上でこのセクションは終了です。
