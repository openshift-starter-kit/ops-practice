# コンテナスターターキット (運用編)
このセクションでは次のハンズオンを実施します。

* [OpenShift Monitoring によるクラスタとアプリケーションの監視](monitoring.md)
* [Role Based Access Control によるユーザの権限管理](rbac.md)
* [ResourceQuota や Limitrange によるアプリケーションのリソース管理](resource_mgmt.md)
